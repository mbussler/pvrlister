
import os, string
from pprint import pprint
from PyQt4 import QtCore, QtGui, QtXml
import subprocess

recPath = "../rec/"

files = []
emptyDirs = []
currentDir = ""
infoFound = True
for ( dirpath, dirnames, filenames) in os.walk(recPath):
  
  if currentDir != dirpath:
    if not infoFound:
      emptyDirs.append(currentDir)
    currentDir = dirpath
    infoFound = False
  
  for filename in filenames:
    if filename == 'info.xml':
      fileAndDir = ( filename, os.path.abspath(dirpath))
      files.append( fileAndDir )
      infoFound = True

print "Following directories have no info.xml included:\n"
for dir in emptyDirs:
  print dir

class XmlHandler(QtXml.QXmlDefaultHandler):
    def __init__(self, root):
        QtXml.QXmlDefaultHandler.__init__(self)
        self._root = root
        self._rootItem = None
        self._item = None
        self._text = ''
        self._parseRecording = False
        self._eventFound = False
        self._startTimeUtc = 0
        self._currentTitle = ''
        self._error = ''

    def startElement(self, namespace, name, qname, attributes):

        if qname == 'Recording':
          self._rootItem = self._item = QtGui.QTreeWidgetItem(self._root)
          self._item.setData(0, QtCore.Qt.UserRole, qname)
          self._item.setText(0, 'Unknown Title')
          self._parseRecording = True
        elif qname == 'Event':
          self._item = QtGui.QTreeWidgetItem(self._item)
          self._item.setData(0, QtCore.Qt.UserRole, qname)
          self._item.setText(0, 'Unknown Title')
          self._item.setExpanded(True)

        self._text = ''
        return True        
        
    def endElement(self, namespace, name, qname):

      if qname == 'StartTimeUtc':
          time = str(int( str(self._text), 16 ))
          if self._parseRecording:
              self._startTimeUtc = time # store timeUtc of recording
              self._parseRecording = False
          elif self._startTimeUtc == time: # check if current event has same timeUtc
              self._eventFound = True
              self._rootItem.setText(0, self._currentTitle)

      elif qname == 'Recording':
          self._parseRecording = False

      elif qname == 'Title' :
          self._currentTitle = self._text
          self._item.setText(0, self._text)

      elif qname == 'ShortInfo':
          self._item.setText(1, self._text)
          if self._eventFound :
              self._rootItem.setText(1, self._text)

      elif qname == 'Duration' :
          self._item.setText(2, self._text)

      elif qname == 'ExtendedInfo':
          self._item.setText(3, self._text)
          if self._eventFound :
              self._rootItem.setText(3, self._text)

      elif qname == 'Service' :
          self._item.setText(4, self._text)

      elif qname == 'Event':
          self._item = self._item.parent()
          if self._eventFound:
                self._eventFound = False

      return True
        
    def characters(self, text):
        self._text += text
        return True

    def fatalError(self, exception):
        print('Parse Error: line %d, column %d:\n  %s' % (
              exception.lineNumber(),
              exception.columnNumber(),
              exception.message(),
              ))
        return False

    def errorString(self):
        return self._error

    def getRootElement(self):
        return self._rootItem

class Window( QtGui.QTreeWidget):
    def __init__(self):
        QtGui.QTreeWidget.__init__(self)
        self.setHeaderLabels(['Title', 'Short Info', 'Duration', 'ExtendedInfo', 'Program', 'Directory', 'info.xml'])
        self.itemDoubleClicked.connect( self.onDoubleClicked )
        self.header().setSortIndicatorShown(True)
        self.header().setClickable(True)
        
        self.header().sectionClicked.connect( self.sortByColumn )

        source = QtXml.QXmlInputSource()
        handler = XmlHandler(self)
        reader = QtXml.QXmlSimpleReader()
        reader.setContentHandler(handler)
        reader.setErrorHandler(handler)

        for (file, dir) in files :
          filename = os.sep.join( [dir, file])
          file = open( filename, 'r')
          xml  = file.read()
          source.setData(xml)
          reader.parse(source)
          handler.getRootElement().setText( 5, dir )
          handler.getRootElement().setText( 6, filename)

        self.sortItems( 0, 0 ) # column 0, ascending
        self.header().setResizeMode( QtGui.QHeaderView.ResizeToContents )
        self.header().setResizeMode( QtGui.QHeaderView.Interactive )


    def onDoubleClicked( self, item, columnIndex ):
        if item is not None:
            text = item.text( columnIndex)
            if columnIndex == 5:
                subprocess.Popen( r'explorer /root,"%s"' % text )
            elif columnIndex == 6:
                subprocess.Popen( r'c:\\Program Files (x86)\\Notepad++\\notepad++.exe "%s"' % text )


if __name__ == '__main__':

    import sys
    app = QtGui.QApplication(sys.argv)
    window = Window()
    window.resize(800, 600)
    window.show()
    sys.exit(app.exec_())