import os, string

recPath = "../rec/"

files = []
emptyDirs = []
currentDir = ""
infoFound = True
for ( dirpath, dirnames, filenames) in os.walk(recPath):
  
  if currentDir != dirpath:
    if not infoFound:
      emptyDirs.append(currentDir)
    currentDir = dirpath
    infoFound = False
  
  for filename in filenames:
    if filename == 'info.xml':
      files.append(os.sep.join([dirpath, filename]))
      infoFound = True

for dir in emptyDirs:
  print dir